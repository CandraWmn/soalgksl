FROM php:7.3.2-apache

COPY php.ini /usr/local/etc/php/

RUN apt-get update && apt-get install -y vim libfreetype6 libjpeg62-turbo-dev libpng-dev mysql-server \
    && docker-php-ext-install pdo pdo_mysql mysqli

COPY . ./

EXPOSE 80
