-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 17, 2019 at 02:27 AM
-- Server version: 10.3.12-MariaDB-1:10.3.12+maria~bionic-log
-- PHP Version: 7.3.2-3+ubuntu18.10.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cookie`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(260) NOT NULL,
  `activated` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `activated`) VALUES
(1, 'lala', '$2y$10$Onoo8q9.z97ZQzI/aILEpuXa0jPDLdlXN0lQcx.LjuUdAT57u/fhW', '0'),
(2, 'olof', '$2y$10$NmSip4ZGgQUBLRdVvEtb/O3KnLzPuSDnj.V8hpJPx7rTJ1CPHaMMy', '0'),
(3, 'agus', '$2y$10$OT45ScJBtaOgRKeJULrre./Ot4IR8YMpLQtDH6KUII6BLtQfH1rBi', '0'),
(7, 'baru', '$2y$10$gfLxkqSCfsErOdZevXHztedU9BGD3XmJwCsPNBPEUs662oGsmYcJ2', '0'),
(15, 'puing', '3da491d737d3e02b1e6273de01baf45b', '0'),
(16, 'jackpot', '4c63008d08e93f5a53fd02196488fca5', '0'),
(17, 'mata', '1c99c5a7d118311c49b120be2118e44a', '0'),
(18, 'bam', 'e5bea9a864d5b94d76ebdaaf43d66f4d', '0'),
(28, 'root', '63a9f0ea7bb98050796b649e85481845', '0'),
(32, 'tara', '2c842d72963554e4ca6286bb120777f6', '0'),
(33, 'ting', '6e75a6fc07caaa04191c4cbe89e9433c', '0'),
(43, 'lp', '351325a660b25474456af5c9a5606c4e', '0'),
(44, 'ti', 'e00fba6fedfb8a49e13346f7099a23fc', '0'),
(51, 'c', '4a8a08f09d37b73795649038408b5f33', '0'),
(53, 'r', '4b43b0aee35624cd95b910189b3dc231', '0'),
(54, 'danu', 'a29e5a0efaa2b1521ebea7cf10cd0eab', '0'),
(55, 'x', '9dd4e461268c8034f5c8564e155c67a6', '0'),
(56, 'oreo', 'd0585ffddbc10793f5e3817424f08fa4', '0'),
(57, 'tre', '7e764aa6b7529530855f0373606d1886', '0'),
(58, 'bolo', 'c8ee78f45a36aa1441fce6c3bf0c7ad9', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
